<?php
$string  = curl_init();


$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];

/*
https://api.sunrise-sunset.org/json?
lat=36.7201600
&lng=-4.4203400
&date=
*/

$url = "https://api.sunrise-sunset.org/json?";
$url .= "lat=" . ($latitude);
$url .= "&lng=" . ($longitude); 
$url .= "&date=today";

curl_setopt($string, CURLOPT_URL, $url);  
curl_setopt($string, CURLOPT_HEADER, 0);
curl_setopt($string, CURLOPT_RETURNTRANSFER, true );
curl_setopt($string,CURLOPT_HEADER, false); 
curl_setopt ($string, CURLOPT_SSL_VERIFYHOST, false); 
curl_setopt ($string, CURLOPT_SSL_VERIFYPEER, false);   
curl_setopt($string, CURLOPT_CUSTOMREQUEST, 'GET');
$result = curl_exec($string);
echo curl_error($string);


print_r($result);

curl_close($string);
?>