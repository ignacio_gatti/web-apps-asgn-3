
function placesSearch(Mychange) {
    
    var latitude = document.getElementById("latitude").innerHTML;
    var longitude = document.getElementById("longitude").innerHTML;
    
    var type = document.getElementById("typeinput").value;
    var radius = document.getElementById("radiusinput").value;

    var checkCoordinates = false;
    var checkTypeRad = false;
    if(type === "" || radius === "")
    {
        alert("You have to fill in both fields");
    }

    else{
        checkTypeRad = true;
    }

    if(latitude === "" || longitude === ""){
        alert("You need to specify a Coordinate first");
    }

    else{
        checkCoordinates = true;
    }
    
    //'url' and 'params' will be sent as a GET request to a PHP page
    var url = "php/cURLPlaces.php";
    var params = "?latitude=" + latitude + "&longitude=" + longitude;
    params += "&type=" + type + "&radius=" + radius; 

    //this will populate the select box 'placeinfo'
    var target = document.getElementById("placeinfo");
    
    var xhr = new XMLHttpRequest();
    var geo_array = [];
    
    var infoWindow = new google.maps.InfoWindow;
    
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            //This will show a message where the user is located
            //provided they allow website to see user's location
            
            infoWindow.open(map);
            //map.setCenter(geo_array[0]);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });

       

    } 
    else {
        
        handleLocationError(false, infoWindow, map.getCenter());
    }


    xhr.onreadystatechange = function () {
        console.log('readyState: ' + xhr.readyState);
        if(xhr.readyState == 2 && checkCoordinates == true && checkTypeRad == true) {
        target.innerHTML = 'Loading...';
        }//end of if statement
        if(xhr.readyState == 4 && xhr.status == 200 && checkCoordinates == true && checkTypeRad == true) {
        
        //takes result from XHR response and parses it
        //to JSON.
        var placeXHR = [JSON.parse(xhr.responseText)];
        //takes the JSON parse and places variables 
        //with the information needed for the for loop below.
        var placeResults = placeXHR[0].results;
        var limit = Object.keys(placeResults).length;
        var increment = 0;

            //takes results from the API and appends them to the
            //select box.
            for(increment=0; increment < limit; increment++){
                var MarkerChange = Mychange;

                if(MarkerChange == undefined){
                MarkerChange = 0;
            }


                var placeNew = document.createElement('option');
                placeNew.className = 'placeNew';
                placeNew.innerHTML = placeResults[increment].name;
                target.appendChild(placeNew);
                target.selectedIndex = MarkerChange;
                var InputMarker = {
                lat: placeResults[increment].geometry.location.lat,
                lng: placeResults[increment].geometry.location.lng

            };

            geo_array.push(InputMarker);
            
            
            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: geo_array[MarkerChange]
        });
        
        

            var marker = new google.maps.Marker({
                position: geo_array[MarkerChange],
                map: map,
                title: placeResults[MarkerChange].name,
                
            });
            
            
            
            
            }//end of for loop

        
        }//end of if statement
    }//end of function
    xhr.open('GET', url + params, true);
    xhr.send();

    
}
function placeChange(){
   var Mychange = document.getElementById('placeinfo').selectedIndex;
//    alert("mytest" + a);
   placesSearch(Mychange);
   
}

