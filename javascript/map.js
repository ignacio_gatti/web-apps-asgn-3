var empty = document.createElement('option');
empty.innerHTML = "";
document.getElementById('prevSearch').appendChild(empty);
document.getElementById('prevSearch').selectedIndex = 0;
//function takes user city and region input and places marker on map
function searchMap() {
    //takes the city and region the user inputs and puts them
    //in variables to be used in the cURL
    var city = document.getElementById("cityinput").value;
    var region = document.getElementById("regioninput").value;

    var checkCityRegion = false;

    if(city === ""){
        alert("You must put in a city first (Region Optional)");
    }

    else{
        checkCityRegion = true;
    }

    //takes the city and region and puts them in parameters
    //and sends them to a php page.
    var url = "php/cURLMaps.php";
    var params = "?city=" + city + "&region=" + region;
    var latitudeTarget = document.getElementById("latitude");
    var longitudeTarget = document.getElementById("longitude");
    var xhr = new XMLHttpRequest();

    //if the request has not been sent properly, the index.html
    //will show up with 'Loading...'
    xhr.onreadystatechange = function() {
    console.log('readyState: ' + xhr.readyState);
      if (xhr.readyState == 2 && checkCityRegion == true) {

          latitudeTarget.innerHTML = 'Loading...';
      }
      //if the request is successful a marker will be placed
      //and the camera will shift to the marker's position.
      if (xhr.readyState == 4 && xhr.status == 200 && checkCityRegion == true) {


          var geolocation = [JSON.parse(xhr.responseText)];
          //displays the latitude and longitude in the index.html page
          latitudeTarget.innerHTML = geolocation[0].latitude;
          longitudeTarget.innerHTML = geolocation[0].longitude;


          //takes the latitude and longitude from the user input
          //this will be used for the marker.
          var InputMarker = {
              lat: geolocation[0].latitude,
              lng: geolocation[0].longitude
          };
          
          //targets the 'map' in index.html.
          //when the marker shows up the camera will shift to where the marker
          //has been placed
          var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 4,
              center: InputMarker
          });
          
          //takes the longitude and latitude from InputMarker
          //and sets a marker there.
          var marker = new google.maps.Marker({
              position: InputMarker,
              map: map
          });
          
          var prev = document.createElement('option');
          prev.innerHTML = city;
          
          document.getElementById('prevSearch').appendChild(prev);
      }
    }
    xhr.open('GET', url + params, true);
    xhr.send();

    
}

function prevSearch(){
    var prSearchOption = document.getElementById('prevSearch').selectedIndex;
    
    document.getElementById("cityinput").value = document.getElementsByTagName("option")[prSearchOption].value
    
    
}

